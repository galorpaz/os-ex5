CC=g++

myCachingFileSystem : myCachingFileSystem.o log.o myCache2DiskFS.o
	$(CC) -g -o myCachingFileSystem myCachingFileSystem.o log.o myCache2DiskFS.o `pkg-config fuse --libs`

myCachingFileSystem.o : myCachingFileSystem.cpp log.h params.h 
	$(CC) -g -Wall  `pkg-config fuse --cflags` -c myCachingFileSystem.cpp 

myCache2DiskFS.o : myCache2DiskFS.cpp log.h params.h 
	$(CC) -g -Wall `pkg-config fuse --cflags` -c myCache2DiskFS.cpp 

log.o : log.cpp log.h params.h
	$(CC) -g -Wall `pkg-config fuse --cflags` -c log.cpp

tar: 
	tar cvf myCachingFileSystem.tar README Makefile myCache2DiskFS.cpp myCache2DiskFS.h myCachingFileSystem.cpp mct.sh fuse.h log.cpp log.h params.h 

clean:
	rm -f myCachingFileSystem *.o *.log


