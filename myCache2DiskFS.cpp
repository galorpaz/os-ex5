#include <ctype.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <fuse.h>
#include <libgen.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/xattr.h>
#include <time.h>
#include <list>
#include <iostream>
#include <string> 
#include <sys/time.h>
#include <string.h>
#include "log.h"
#include "myCache2DiskFS.h"
#include "params.h"

#define MY_MAGIC 0x84758937

using namespace std;
/*
 * for each file that was asked to open this struct is initiallized
 * block_map is used to map blocks of the file to a cache block 
 */
typedef struct {
	unsigned magic; // flag that means this struct was created
	char  path[PATH_MAX];	// the path of the file represented by this file
	int fd; // the fd of the file
	int  block_map[1]; // a block_map- map each file_block to the cache_block that holds
} file_block_descriptor;
/*
 * this struct represent a cache block 
 */
typedef struct {
	time_t   acces_time_msec;
	time_t	 acces_time_sec;
	unsigned char * data; // that data of the file_block
	unsigned data_length; //the length of the data was read from the file
	int* used_block_map;  // this point to the file_block_decriptor, when the file_block
						  // descriptor is free this pointer point to null, that way we 
						  // can know that the file holded by the cache was closed		
	char path[PATH_MAX];  // the path of the file holded by this cache_block
	int	  file_block_idx; // the file_block_idx holded by the cache_block
} cache_block_data;

/*
 * returns the fd of the file represented by lbd (file_block_descriptor)
 */
int cache_get_fd(unsigned long lbd) {
	file_block_descriptor* bd = (file_block_descriptor*)lbd;
	if (bd == NULL || bd->magic != MY_MAGIC) {
		return -1;	
	}
	return bd->fd;
}
/*
 * list of string, the string represent the path of the each open file 
 */
list<string> open_files;
/*
 * the cache
 */
static cache_block_data *block_cache = NULL;
/*
 * return the current time using gettimeofday() in miliseconds.
 * return the seconds through the p_sec given as an argumrnt 	
 */
static unsigned _getCurTime(time_t *p_sec=NULL) {
  struct timeval tv;

  if (gettimeofday(&tv, NULL) < 0) 
    return 0;
	if (p_sec != NULL)
		*p_sec = tv.tv_sec;
  return (unsigned)(tv.tv_sec * 1000u + tv.tv_usec/1000u);
}
/*
 * return the name of the file from a given path,
 * it cuts the file form it path.	
 */
const char * f_local_name(const char * f_path)
{
	int len = strlen(f_path) -1;
	
	for (; len >=0; --len) {
		//if (f_path[len] == '/')
		if (strncmp(f_path,"/tmp/rootdir/",len)==0)			
			break;
	}
	return &f_path[len];

}
const char * f_name(const char * f_path)
{
	int len = strlen(f_path) -1;
	
	for (; len >=0; --len) {
		if (f_path[len] == '/')
			break;
	}
	return &f_path[len];

}
/*
 * this function write to the ioctloutput.log the time in the specified format 
 */
void write_formated_time(FILE *file, time_t sec, time_t msec)
{
	static const char* months[]={"01","02","03","04","05","06","07","08","09","10","11","12"};
	static const char* days[]={"01","02","03","04","05","06","07","08","09","10","11","12"
								,"13","14","15","16","17","18","19","20","21","22","23","24"
								"25","26","27","28","29","30","31"};
	struct tm      *tm;
	tm = localtime(&sec);
	ioctl_msg(file, "\t%s:%s:%02u:%02u:%02u:%03u\n",
		months[tm->tm_mon],
		days[tm->tm_mday-1],
		tm->tm_hour,
		tm->tm_min,
		tm->tm_sec,
		msec);
}


/*
 * wirte the info of each cache block into the ioctloutput.log 
 * 
 */
void  write_cache_block_info(FILE * file)
{
//	log_msg("!!!!!!!!!!!!! 1 write_cache_block_info!!!!!!!!!!!!!!!!!\n");		
	unsigned c,d, swap;	
	unsigned size =MY_CACHE_DATA->numBlocks; 	
	int array[size];	
	time_t delta_time;	
	//initiallize the array with the indexes of the accupied blocks
	c=0;
	d=0;
	while (d<size) {
		if (block_cache[d].file_block_idx >= 0)	{	
		   array[c] = d;
			c++;
		}
		d++;
		//log_msg("d is now: %d\n", d);
	}
	//log_msg("!!!!!!!!!!!!! 2 write_cache_block_info!!!!!!!!!!!!!!!!!\n");		
	if ( c < size ) {
		while (c<size) {
			array[c] = -1;
			c++;		
		}
	}

	//sorting the array according to the access time of the block insex int the cache
	for (c = 0 ; c < ( size - 1 ); c++)
  	{
		if (array[c]==-1){
			break;		
		}    	
		for (d = 0 ; d < size - c - 1; d++)
    	{
		    if (array[d+1] == -1) {
				break;
			}
			delta_time = block_cache[array[d+1]].acces_time_msec - block_cache[array[d]].acces_time_msec;
			if (delta_time>0) // For decreasing order use < 
      		{
        		swap       = array[d];
        		array[d]   = array[d+1];
        		array[d+1] = swap;
      		}
    	}
  	}

	for (c=0; c<size; ++c) {
		if (array[c]!=-1) {
			//log_msg("array[%d] = %d\n",c,array[c]);
			ioctl_msg(file, "%s\t%u", f_local_name(block_cache[array[c]].path), block_cache[array[c]].file_block_idx+1);
			//print the line of time in the specified format	
		 	write_formated_time(file, block_cache[array[c]].acces_time_sec, block_cache[array[c]].acces_time_msec%1000U);
		}
		else 
			ioctl_msg(file, "un-mapped cache block %u\n", c+1);	
	}
	
	
}
/*
 * this function return the cache idx of the least recently used base on the acces_time 
 * value, after it found available cache block it initialize it
 * params : path - the path of the file
 * 			file-blk-idx - the number of the file_blk
 *			p_block_map - the pointr to the block in cache where the file block will be
 * 							mapped to.		
 */
static void get_lru_block_idx(const char* path, int file_blk_idx, int * p_block_map)
{
	/* first it get the current time. Iterating over the cache, if the cahce block is free
     * it use it, secondly it maximaize the delta_time
	 */
	unsigned nBlocks = MY_CACHE_DATA->numBlocks;
	unsigned i;
	int lru_block = -1;	
	time_t cur_time = _getCurTime();
	time_t delta_time = 0; // difference of curren_time and access_time of each cache block 
	log_msg("Enter get_lru_block_idx(numBlk=%u)\n",nBlocks); 	
	for (i=0; i<nBlocks; ++i)
	{
		if (block_cache[i].file_block_idx == -1)
		{ // the cache block is free
			lru_block = i;
			break;
		}

		if ((cur_time - block_cache[i].acces_time_msec) > delta_time)
		{ // maximum value of delta_time was found
			lru_block = i;
			delta_time = (cur_time - block_cache[i].acces_time_msec);
		}
	}
	if (block_cache[lru_block].used_block_map != NULL) 
		// means the cache block is currently holded by already open file								 
		*block_cache[lru_block].used_block_map = -1;
	//update the fields of the cahce in the found lru_block	
	block_cache[lru_block].used_block_map = p_block_map; //the file_block number
	strcpy(block_cache[lru_block].path, path);
	block_cache[lru_block].file_block_idx = file_blk_idx;	
	if (p_block_map != NULL)
		*p_block_map = lru_block; // now it is pointing to the new block in cache
 	log_msg("Exit get_lru_block_idx()	lru_block=%u 	acces_time=%u\n", lru_block, block_cache[lru_block].acces_time_msec);
	return ;
}
/*
 * assaign the data from the file to block_cache at cache_blk_idx possition
 */
static int read_block_from_file(int fd, int file_blk_idx ,int cache_blk_idx) 
{
	char local[10000]={0};	
	off_t offset = file_blk_idx*MY_CACHE_DATA->blockSize;	
	int data_length = pread(fd, block_cache[cache_blk_idx].data, MY_CACHE_DATA->blockSize, offset);	
	if (data_length < 0) {
		err_msg("pread failed");
		ioctl_msg(NULL, "error in read_block_from_file\n");
		data_length = 0;
	}
	block_cache[cache_blk_idx].data_length = data_length;	
	block_cache[cache_blk_idx].acces_time_msec = _getCurTime(&block_cache[cache_blk_idx].acces_time_sec);
	// this for debug
	memcpy(local, block_cache[cache_blk_idx].data, data_length);
	local[data_length] = 0;
	log_msg("read_block_from_file(file_blk_idx=%d cache_blk_idx=%d length=%d) data:%s\n", 
		file_blk_idx , cache_blk_idx, data_length, local);
	return data_length;
}
/**
 * put in buf the asked data, from block_idx, data size num of blocks, start from the 
 * offset byte.
 * return the number of bytes it read.
 * if the data_size requsted is greater then blockSize-offset, then it reads the bytes 
 * until the end of the block,
 * and this is basically the only case were data_read < data_size.
 */
static int read_data_from_cache(int block_idx, char *buf, size_t data_size, off_t offset)
{
	unsigned data_read = block_cache[block_idx].data_length - offset;	
	if (data_read > data_size) {
		data_read = data_size;
	}	
	memcpy(buf, block_cache[block_idx].data+offset, data_read);	
	return data_read;
}
/**
 * this function is called when file is close and the file_block_descriptor
 * is needed to be deleted,
 * this function iterates over the given block_map
 * that originally belong to some file_block_descriptor that needs to be release. 
 * check what is the cache block that were mapped to it, and put NULL in the 
 * block_cache.used_block_map   
 */
void releas_cache_block_map(int block_map[])
{
	unsigned i;
	int blk_idx;
	for (i=0; i < MY_CACHE_DATA->numBlocks; ++i)
	{
		if ((blk_idx=block_map[i]) >= 0) {	
			log_msg("release cache block %d from file %s", blk_idx, block_cache[blk_idx].path);
			block_cache[blk_idx].used_block_map = NULL;
		}
	}
}
/**
 * this function is called when a file was closed and was asked to open again,
 * its purpose is to find the cache_block that still contains data of this file
 * and to remmap those file blocks, so the file_block_descriptor represnting the open file
 * will know which blocks are already in the cache, this data is updated in the given 
 * block_map.
 * its iterate over the cache blocks and check if it contains data from the file 
 * asked to be open from fpath.	
 */
void remap_cache_block_map(const char * fpath, int block_map[])
{
	unsigned i;
	int file_blk_idx;
	for (i=0; i < MY_CACHE_DATA->numBlocks; ++i)
	{
		file_blk_idx = 	block_cache[i].file_block_idx;

		if ( (block_cache[i].path[0] != '\0') && (file_blk_idx >= 0) ) {
			
			if (strcmp(block_cache[i].path, fpath) == 0) {
				//after it found a block_cache containing data from the file it update the
   				// block_map
				block_map[file_blk_idx] = i;
				block_cache[i].used_block_map = &block_map[file_blk_idx];			
				log_msg("remap cache block %d for file %s file_block: %d\n", i,  f_name(block_cache[i].path), file_blk_idx);
			}
		}
	}
}

/**
 * Initialize filesystem
 *
 * The return value will passed in the private_data field of
 * fuse_context to all file operations and as a parameter to the
 * destroy() method.
 *
 * Introduced in version 2.3
 * Changed in version 2.6
 */
void *cache_init(struct fuse_conn_info *conn)
{
	unsigned i;    
	log_msg("\nmyCache_init() numBlocks=%u blockSize=%u\n", MY_CACHE_DATA->numBlocks, MY_CACHE_DATA->blockSize);
	
    if (MY_CACHE_DATA->numBlocks == 0 || MY_CACHE_DATA->blockSize == 0) {
		err_msg("numBlocks or blockSize is zero");
		ioctl_msg(NULL, "error in cache_init\n");
		return NULL;
	}

	block_cache = (cache_block_data *) malloc(sizeof(cache_block_data) * MY_CACHE_DATA->numBlocks);	
	if (block_cache == NULL) {
		err_msg("faild to alloc block_cache");
		ioctl_msg(NULL, "error in cache_init\n");
		return NULL;
	}
	//initialize the block_cache with deafult values
	memset(block_cache, 0, sizeof(cache_block_data) * MY_CACHE_DATA->numBlocks);	

	for(i=0; i<MY_CACHE_DATA->numBlocks; ++i)
	{
									
		posix_memalign((void **) &block_cache[i].data, sizeof(unsigned char *), MY_CACHE_DATA->blockSize);
		if (block_cache[i].data == NULL) {
			err_msg("faild to alloc block_cache block array");
			ioctl_msg(NULL, "error in cache_init\n");
			return NULL;
		}
		block_cache[i].acces_time_msec = _getCurTime();
		block_cache[i].used_block_map = NULL;
		block_cache[i].path[0] = '\0';
		block_cache[i].file_block_idx = -1;
	}
	 	log_msg("\nExit myCache_init()\n");
	
    return MY_CACHE_DATA;
}
/**
 * free block_cache 
 */
void cache_destroy() {
	if (block_cache == NULL) return;	
	unsigned i;
	for (i=0; i<MY_CACHE_DATA->numBlocks; ++i) {
		free(block_cache[i].data);	
	}
	free(block_cache);
	block_cache = NULL;
}
/**
 * check if the file specified in path is already in the cache,
 * return the position in the list (>=0), else return -1
 */
int file_in_cache(const char* path) {
	log_msg("enter file_in_cache with path: %s\n", path);
    string str_path = string(path);
	list<string>::iterator it ;
	int ret_val = -1;
	int i = 0;
	for(it=open_files.begin(); it!=open_files.end(); ++it) {
		++i;		
		if (str_path.compare(*it) == 0)
			ret_val = i;
	}
	log_msg("ewit file_in_cache with ret_val: %d\n", ret_val);	
	return ret_val;
}
/**
 * this function can be called only if path is in the list
 * it removes the path from open_files list  
 */
void remove_file_from_list(const char* path) {
	log_msg("enter remove_file_from_list with path: %s\n", path);
    string str_path = string(path);
	open_files.remove(str_path);
	return;
}
/*
 * this function open the file located in path with the requested flags
 */
long cache_open(const char* path, int flags) {
	unsigned i;
	unsigned nBlocks = MY_CACHE_DATA->numBlocks;
	if (file_in_cache(path)>=0) { 
		//file already exist in list
		return 0;
	}
	//create file_block_descriptor for the file
	file_block_descriptor *bd= (file_block_descriptor *) malloc(sizeof(file_block_descriptor) + nBlocks*sizeof(int));
	if (bd == NULL)
		return -1;
	for (i=0; i<nBlocks; ++i){
		bd->block_map[i] = -1;
	}
    // remove the O_DIRECT which couses problems!!!
	flags &= ~O_DIRECT;
	// open the file and update the fd field
	bd->fd = open(path, flags | O_SYNC);
	
	//check if failed to open the file
	if (bd->fd < 0) {
		free(bd);
		return -1;
	}
    //update the file_block_descriptor
	strcpy(bd->path , path);
	bd->magic = MY_MAGIC;
	log_msg("enter cache_open with path: %s\n", path);	
	open_files.push_back(string(path));
	remap_cache_block_map(path, bd->block_map); //remapping if needed
	return (unsigned long) bd;
}
/**
 * this function is called from fuse_read.
 * its read the file to the cache in case that the file_blocks are not in the cache
 * put in buf the requested data
 */
int cache_read(unsigned long l_bd, char *buf, size_t size, off_t offset)
{
	unsigned read_data = 0;	
	unsigned numBlocks = MY_CACHE_DATA->numBlocks;
	unsigned blockSize = MY_CACHE_DATA->blockSize;
	unsigned blk_idx;
	unsigned blk_offset = offset % blockSize;
	int length;
	char * org_buf = buf;
	file_block_descriptor *bd=(file_block_descriptor *)l_bd;
	log_msg("enter cache_read from bd=%p magic=%X size=%u\n", bd, bd->magic, size);
	if (bd == NULL || bd->magic !=MY_MAGIC) {
		err_msg("cache_read wrong block descriptor\n");
		ioctl_msg(NULL, "error in cache_read\n");
		return -1; 
	}
    //find the file block that is required
	blk_idx = offset / blockSize;
	log_msg("cache_read(starting from blk_idx%d)\n", blk_idx);
	if (blk_idx >= numBlocks) {
		err_msg("file:%s size greater then cache size" );
		ioctl_msg(NULL, "error in cache_read\n");
		return -1;
	}
	do {
		log_msg("block_map[%d]=%d\n", blk_idx, bd->block_map[blk_idx]);
		if (bd->block_map[blk_idx] == -1) { // if the file block is not in the cache
			get_lru_block_idx(bd->path,blk_idx, &bd->block_map[blk_idx]); 
			// return index of avilable block in the cache according to LRU system
			if (bd->block_map[blk_idx] == -1 || bd->block_map[blk_idx] >= (int)numBlocks) {
				err_msg("logic error at get_lru_block");
				ioctl_msg(NULL, "error in cache_read\n");
				return -1;
			}
			read_block_from_file(bd->fd, blk_idx, bd->block_map[blk_idx]); //assaign the file block in to the cache block 
		}
		length = read_data_from_cache(bd->block_map[blk_idx], buf, size-read_data, blk_offset); 
		// return (size-read_data) bytes from (bd->cache_block_idx) into buf 
		if (length < 0){
			err_msg("cache_read failed");
			ioctl_msg(NULL, "error in cache_read\n");
			read_data = -1;		
			break;
		}
		if (length == 0) {
			log_msg("cache_read reached EOF\n");
			break;	
		}
		buf[length] = 0;
		log_msg("data read from cache file_blk=%d  cache_blk=%d len=%d data:%s\n",blk_idx, bd->block_map[blk_idx], length, buf);
		read_data += length;
		buf += length;
 		
		blk_offset = 0;
		if (read_data < size) { // there is more data to read
			blk_idx++;
			offset += length;	
		}
	} while(read_data < size);

	log_msg("Exit cache_read: length: %d  data: %s\n", read_data, org_buf);
	return read_data;
}

/*
 * when file is asked to close, this function release the file_block_descriptor
 */
int cache_close(unsigned long lbd)
{
	file_block_descriptor *bd=(file_block_descriptor *) lbd;
	if (bd == NULL || bd->magic != MY_MAGIC) {
		err_msg("cache_close wrong block descriptor");
		ioctl_msg(NULL, "error in cache_close\n");
		return -1; 
	}
	close(bd->fd);
	releas_cache_block_map(bd->block_map);

	remove_file_from_list(bd->path);
	free((void*) bd);
	return 0;
}
/**
 * this function search if there are block in the cache from the file that was renamed.
 * this can be done only when the file is close, but there are still blocks in the cache 
 */
void rename_file_in_cache(char* fpath, char* fnewpath){
	unsigned i;
    unsigned numBlocks = MY_CACHE_DATA->numBlocks;
	unsigned old_path_len = strlen(fnewpath);	
	char * file_name = (char*)f_name(fpath);	
	if (block_cache == NULL) return;		
	log_msg("Entered rename_file_in_cache: path is: %s new path is: %s\n", fpath, fnewpath);	
	for (i=0; i< numBlocks; ++i) {
		if (strcmp(block_cache[i].path,fpath) == 0) {
			//renaming a file		
			strcpy(block_cache[i].path, fnewpath);		
		}
		else if (strncmp(block_cache[i].path,fpath,old_path_len) == 0) {
			log_msg("rename for dorectory: path is: %s new path is: %s\n", fpath, fnewpath);				
			//renaming directory in the root directory
			file_name = (char*)f_name(block_cache[i].path);				
			strcpy(block_cache[i].path, fnewpath);
			strcat(block_cache[i].path,file_name); 		
		}
	}
	return;
}

