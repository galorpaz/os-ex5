/*
  Copyright (C) 2012 Joseph J. Pfeiffer, Jr., Ph.D. <pfeiffer@cs.nmsu.edu>

  This program can be distributed under the terms of the GNU GPLv3.
  See the file COPYING.

  There are a couple of symbols that need to be #defined before
  #including all the headers.
*/

#ifndef _PARAMS_H_
#define _PARAMS_H_

// The FUSE API has been changed a number of times.  So, our code
// needs to define the version of the API that we assume.  As of this
// writing, the most current API version is 26
#ifndef FUSE_USE_VERSION
	#define FUSE_USE_VERSION 26
#endif
// need this to get pwrite().  I have to use setvbuf() instead of
// setlinebuf() later in consequence.
#ifndef FUSE_USE_VERSION
	#define _XOPEN_SOURCE 500
#endif
// maintain bbfs state in here
#include <limits.h>
#include <stdio.h>
struct myCache_state {
    FILE *logfile;
    char *rootdir;
	unsigned 	numBlocks;
	unsigned    blockSize;

};
#define MY_CACHE_DATA ((struct myCache_state *) fuse_get_context()->private_data)

#endif
