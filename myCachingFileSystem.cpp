#include "params.h"
#include <ctype.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <fuse.h>
#include <libgen.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/xattr.h>
#include <time.h>
#include <sys/time.h>
#include "myCache2DiskFS.h"
#include "log.h"

//#define  MY_DEBUG

static struct fuse_operations caching_oper;

static int bb_error(char* str)
{
    int ret = -errno;
    
    log_msg("    ERROR %s: %s\n", str, strerror(errno));
    
    return ret;
}
static void fuse_fullpath(char fpath[PATH_MAX], const char *path)
{
    strcpy(fpath, MY_CACHE_DATA->rootdir);
    strncat(fpath, path, PATH_MAX); // ridiculously long paths will
				    // break here

    log_msg("    fuse_fullpath:  rootdir = \"%s\", path = \"%s\", fpath = \"%s\"\n",
	    MY_CACHE_DATA->rootdir, path, fpath);
}
/**
 * Ioctl
 *
 * flags will have FUSE_IOCTL_COMPAT set for 32bit ioctls in
 * 64bit environment.  The size and direction of data is
 * determined by _IOC_*() decoding of cmd.  For _IOC_NONE,
 * data will be NULL, for _IOC_WRITE data is out area, for
 * _IOC_READ in area and if both are set in/out area.  In all
 * non-NULL cases, the area is of _IOC_SIZE(cmd) bytes.
 *
 * Introduced in version 2.8
 */
int fuse_ioctl (const char *, int, void *, struct fuse_file_info *, unsigned, void *data) {
	char fpath[PATH_MAX];
	FILE* cache_log=NULL;
	timeval tv;	

    fuse_fullpath(fpath, "/../ioctloutput.log");
    log_msg("\nfuse_ioctl(path=\"%s\")\n", fpath);
    
	cache_log = fopen (fpath, "a+");
	if (cache_log == NULL) {
		fprintf(stderr, "system error: couldn't open ioctloutput.log file\n");
	}	
	log_msg("fopen(%s) has done\n", fpath);
	gettimeofday(&tv, NULL);
	//print the first line of time		
	write_formated_time(cache_log, tv.tv_sec, tv.tv_usec/1000L);
	//print the information of each cache block	
	write_cache_block_info(cache_log);	
	fclose(cache_log);
		
	return 0;
}

/** Get file attributes.
 *
 * Similar to stat().  The 'st_dev' and 'st_blksize' fields are
 * ignored.  The 'st_ino' field is ignored except if the 'use_ino'
 * mount option is given.
 */
int fuse_getattr(const char *path, struct stat *statbuf)
{
    int retstat = 0;
    char fpath[PATH_MAX];
    
    log_msg("\nfuse_getattr(path=\"%s\", statbuf=0x%08x)\n",
	  path, statbuf);
    fuse_fullpath(fpath, path);
    
    retstat = lstat(fpath, statbuf);
    if (retstat != 0) {
		retstat = err_msg("fuse_getattr lstat\n");
		ioctl_msg(NULL, "error in fuse_getattr\n");
	}
    log_stat(statbuf);
    
    return retstat;
}

/** Rename a file */
// both path and newpath are fs-relative
int fuse_rename(const char *path, const char *newpath)
{ 
    int retstat = 0;
    char fpath[PATH_MAX];
    char fnewpath[PATH_MAX];
    
    log_msg("\nfuse_rename(fpath=\"%s\", newpath=\"%s\")\n",
	    path, newpath);
    fuse_fullpath(fpath, path);
    fuse_fullpath(fnewpath, newpath);
		
    
    if (retstat < 0) {
		retstat = err_msg("fuse_rename rename");
		ioctl_msg(NULL, "error in fuse_rename\n");
	}	
	if (file_in_cache(fpath) > 0) 
	{ // file is open
	 	err_msg("file %s is open\n", fpath);
		ioctl_msg(NULL, "error in fuse_rename\n");
		return retstat;	
	} else { //file is closed
		retstat = rename(fpath, fnewpath);		
		log_msg("file %s is rename to: %s\n", fpath, fnewpath);    	
		rename_file_in_cache(fpath, fnewpath);
    }    
	//fuse_ioctl(NULL, 0, NULL, NULL, 0, NULL);
    return retstat;
}

/** Create a hard link to a file */
int fuse_link(const char *path, const char *newpath)
{
    int retstat = 0;
    char fpath[PATH_MAX], fnewpath[PATH_MAX];
    
    log_msg("\nfuse_link(path=\"%s\", newpath=\"%s\")\n",
	    path, newpath);
    fuse_fullpath(fpath, path);
    fuse_fullpath(fnewpath, newpath);
    
    retstat = link(fpath, fnewpath);
    if (retstat < 0){
		retstat = err_msg("fuse_link link");
		ioctl_msg(NULL, "error in fuse_link\n");
    }
    return retstat;
}

/** Change the permission bits of a file */
int fuse_chmod(const char *path, mode_t mode)
{
    int retstat = 0;
    char fpath[PATH_MAX];
    
    log_msg("\nfuse_chmod(fpath=\"%s\", mode=0%03o)\n",
	    path, mode);
    fuse_fullpath(fpath, path);
    
    retstat = chmod(fpath, mode);
    if (retstat < 0)
	retstat = err_msg("fuse_chmod chmod");
	ioctl_msg(NULL, "error in fuse_getattr\n");
    
    return retstat;
}

/** Change the owner and group of a file */
int fuse_chown(const char *path, uid_t uid, gid_t gid)
  
{
    int retstat = 0;
    char fpath[PATH_MAX];
    
    log_msg("\nfuse_chown(path=\"%s\", uid=%d, gid=%d)\n",
	    path, uid, gid);
    fuse_fullpath(fpath, path);
    
    retstat = chown(fpath, uid, gid);
    if (retstat < 0){ 
		retstat = err_msg("fuse_chown chown");
		ioctl_msg(NULL, "error in fuse_chown\n");    
	}
    return retstat;
}

/** Change the size of a file */
int fuse_truncate(const char *path, off_t newsize)
{
    int retstat = 0;
    char fpath[PATH_MAX];
    
    log_msg("\nfuse_truncate(path=\"%s\", newsize=%lld)\n",
	    path, newsize);
    fuse_fullpath(fpath, path);
    
    retstat = truncate(fpath, newsize);
    if (retstat < 0) {
		err_msg("fuse_truncate truncate");
		ioctl_msg(NULL, "error in fuse_truncate\n");    
	}
    return retstat;
}

/** Change the access and/or modification times of a file */
int fuse_utime(const char *path, struct utimbuf *ubuf)
{
    int retstat = 0;
    char fpath[PATH_MAX];
    
    log_msg("\nfuse_utime(path=\"%s\", ubuf=0x%08x)\n",
	    path, ubuf);
    fuse_fullpath(fpath, path);
    
    retstat = utime(fpath, ubuf);
    if (retstat < 0) {
		retstat = err_msg("fuse_utime utime");
		ioctl_msg(NULL, "error in fuse_utime\n");	
	}
    
    return retstat;
}

/** File open operation
 *
 * No creation, or truncation flags (O_CREAT, O_EXCL, O_TRUNC)
 * will be passed to open().  Open should check if the operation
 * is permitted for the given flags.  Optionally open may also
 * return an arbitrary filehandle in the fuse_file_info structure,
 * which will be passed to all file operations.
 *
 * Changed in version 2.2
 */
int fuse_open(const char *path, struct fuse_file_info *fi)
{
    int retstat = 0;
    long bd;
    char fpath[PATH_MAX];
    
    log_msg("\nfuse_open(path=\"%s\", fi=0x%08x)\n",
	    path, fi);
    fuse_fullpath(fpath, path);
   	// if the file is already open return error
	log_msg("call cache_open path=%s\n", fpath);
    bd = cache_open(fpath, fi->flags);
    if (bd == 0) {
		retstat = -1;
		err_msg("fuse_open file:%s already opend",fpath);
		ioctl_msg(NULL, "error in fuse_open\n");
	}
    else
	if (bd == -1) {
		retstat = -1;
		err_msg("fuse_open  error at opening file: %s",fpath);
		ioctl_msg(NULL, "error in fuse_open\n");
	}
	else {
    	fi->fh =(uint64_t) ((unsigned long) bd);
		log_msg("fuse_open  success fh=0x%08X\n",fi->fh);
	}
	    
	log_fi(fi);
    
    return retstat;
}

/** Read data from an open file
 *
 * Read should return exactly the number of bytes requested except
 * on EOF or error, otherwise the rest of the data will be
 * substituted with zeroes.  An exception to this is when the
 * 'direct_io' mount option is specified, in which case the return
 * value of the read system call will reflect the return value of
 * this operation.
 *
 * Changed in version 2.2
 */
int fuse_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    int retstat = 0;
    
    log_msg("\nfuse_read(path=\"%s\", buf=0x%08x, size=%d, offset=%lld, fi=0x%08x)\n",
	    path, buf, size, offset, fi);
    // no need to get fpath on this one, since I work from fi->fh not the path
    log_fi(fi);
    
    retstat = cache_read((unsigned long)fi->fh, buf, size, offset);
    if (retstat < 0){
		retstat = err_msg("fuse_read read");
		ioctl_msg(NULL, "error in fuse_read\n");	
	}
   //fuse_ioctl(NULL, 0, NULL, NULL, 0, NULL);
    return retstat;
}

/** Get file system statistics
 *
 * The 'f_frsize', 'f_favail', 'f_fsid' and 'f_flag' fields are ignored
 *
 * Replaced 'struct statfs' parameter with 'struct statvfs' in
 * version 2.5
 */
int fuse_statfs(const char *path, struct statvfs *statv)
{
    int retstat = 0;
    char fpath[PATH_MAX];
    
    log_msg("\nfuse_statfs(path=\"%s\", statv=0x%08x)\n",
	    path, statv);
    fuse_fullpath(fpath, path);
    
    // get stats for underlying filesystem
    retstat = statvfs(fpath, statv);
    if (retstat < 0)
	retstat = err_msg("fuse_statfs statvfs");
    
    log_statvfs(statv);
    
    return retstat;
}

/** Possibly flush cached data
 *
 * BIG NOTE: This is not equivalent to fsync().  It's not a
 * request to sync dirty data.
 *
 * Flush is called on each close() of a file descriptor.  So if a
 * filesystem wants to return write errors in close() and the file
 * has cached dirty data, this is a good place to write back data
 * and return any errors.  Since many applications ignore close()
 * errors this is not always useful.
 *
 * NOTE: The flush() method may be called more than once for each
 * open().  This happens if more than one file descriptor refers
 * to an opened file due to dup(), dup2() or fork() calls.  It is
 * not possible to determine if a flush is final, so each flush
 * should be treated equally.  Multiple write-flush sequences are
 * relatively rare, so this shouldn't be a problem.
 *
 * Filesystems shouldn't assume that flush will always be called
 * after some writes, or that if will be called at all.
 *
 * Changed in version 2.2
 */
int fuse_flush(const char *path, struct fuse_file_info *fi)
{
    int retstat = 0;
    
    log_msg("\nfuse_flush(path=\"%s\", fi=0x%08x)\n", path, fi);
    // no need to get fpath on this one, since I work from fi->fh not the path
    log_fi(fi);
	
    return retstat;
}

/** Release an open file
 *
 * Release is called when there are no more references to an open
 * file: all file descriptors are closed and all memory mappings
 * are unmapped.
 *
 * For every open() call there will be exactly one release() call
 * with the same flags and file descriptor.  It is possible to
 * have a file opened more than once, in which case only the last
 * release will mean, that no more reads/writes will happen on the
 * file.  The return value of release is ignored.
 *
 * Changed in version 2.2
 */
int fuse_release(const char *path, struct fuse_file_info *fi)
{
    int retstat = 0;
    
    log_msg("\nfuse_release(path=\"%s\", fi=0x%08x)\n",  path, fi);
    log_fi(fi);

    // We need to close the file.  Had we allocated any resources
    // (buffers etc) we'd need to free them here as well.
    retstat = cache_close(fi->fh);
    
    return retstat;
}

/** Open directory
 *
 * This method should check if the open operation is permitted for
 * this  directory
 *
 * Introduced in version 2.3
 */
int fuse_opendir(const char *path, struct fuse_file_info *fi)
{
    DIR *dp;
    int retstat = 0;
    char fpath[PATH_MAX];
    
    log_msg("\nfuse_opendir(path=\"%s\", fi=0x%08x)\n",
	  path, fi);
    fuse_fullpath(fpath, path);
    
    dp = opendir(fpath);
    if (dp == NULL) {
		retstat = err_msg("fuse_opendir opendir");
    	ioctl_msg(NULL, "error in fuse_opendir\n");
	}
    fi->fh = (intptr_t) dp;
    
    log_fi(fi);
    
    return retstat;
}

/** Read directory
 *
 * This supersedes the old getdir() interface.  New applications
 * should use this.
 *
 * The filesystem may choose between two modes of operation:
 *
 * 1) The readdir implementation ignores the offset parameter, and
 * passes zero to the filler function's offset.  The filler
 * function will not return '1' (unless an error happens), so the
 * whole directory is read in a single readdir operation.  This
 * works just like the old getdir() method.
 *
 * 2) The readdir implementation keeps track of the offsets of the
 * directory entries.  It uses the offset parameter and always
 * passes non-zero offset to the filler function.  When the buffer
 * is full (or an error happens) the filler function will return
 * '1'.
 *
 * Introduced in version 2.3
 */
int fuse_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset,
	       struct fuse_file_info *fi)
{
    int retstat = 0;
    DIR *dp;
    struct dirent *de;
    
    log_msg("\nfuse_readdir(path=\"%s\", buf=0x%08x, filler=0x%08x, offset=%lld, fi=0x%08x)\n",
	    path, buf, filler, offset, fi);
    // once again, no need for fullpath -- but note that I need to cast fi->fh
    dp = (DIR *) (uintptr_t) fi->fh;

    // Every directory contains at least two entries: . and ..  If my
    // first call to the system readdir() returns NULL I've got an
    // error; near as I can tell, that's the only condition under
    // which I can get an error from readdir()
    de = readdir(dp);
    if (de == 0) {
		retstat = err_msg("fuse_readdir readdir");
		ioctl_msg(NULL, "error in fuse_readdir\n");
		return retstat;
    }

    // This will copy the entire directory into the buffer.  The loop exits
    // when either the system readdir() returns NULL, or filler()
    // returns something non-zero.  The first case just means I've
    // read the whole directory; the second means the buffer is full.
    do {
		log_msg("calling filler with name %s\n", de->d_name);
		if (filler(buf, de->d_name, NULL, 0) != 0) {
	    		log_msg("    ERROR fuse_readdir filler:  buffer full");
	    	return -ENOMEM;
		}
    } while ((de = readdir(dp)) != NULL);
    
    log_fi(fi);
    
    return retstat;
}

/** Release directory
 *
 * Introduced in version 2.3
 */
int fuse_releasedir(const char *path, struct fuse_file_info *fi)
{
    int retstat = 0;
    
    log_msg("\nfuse_releasedir(path=\"%s\", fi=0x%08x)\n",
	    path, fi);
    log_fi(fi);
    
    closedir((DIR *) (uintptr_t) fi->fh);
    
    return retstat;
}


/**
 * Clean up filesystem
 *
 * Called on filesystem exit.
 *
 * Introduced in version 2.3
 */
void fuse_destroy(void *userdata)
{
    log_msg("\nfuse_destroy(userdata=0x%08x)\n", userdata);
	cache_destroy();
}

/**
 * Check file access permissions
 *
 * This will be called for the access() system call.  If the
 * 'default_permissions' mount option is given, this method is not
 * called.
 *
 * This method is not called under Linux kernel versions 2.4.x
 *
 * Introduced in version 2.5
 */
int fuse_access(const char *path, int mask)
{
    int retstat = 0;
    char fpath[PATH_MAX];
   
    log_msg("\nfuse_access(path=\"%s\", mask=0%o)\n",
	    path, mask);
    fuse_fullpath(fpath, path);
    
    retstat = access(fpath, mask);
    
    if (retstat < 0) {
		retstat = err_msg("fuse_access access");
    	ioctl_msg(NULL, "error in fuse_access\n");
	}
    return retstat;
}

/**
 * Get attributes from an open file
 *
 * This method is called instead of the getattr() method if the
 * file information is available.
 *
 * Currently this is only called after the create() method if that
 * is implemented (see above).  Later it may be called for
 * invocations of fstat() too.
 *
 * Introduced in version 2.5
 */
int fuse_fgetattr(const char *path, struct stat *statbuf, struct fuse_file_info *fi)
{
    int retstat = 0;
	int fd;
    
    log_msg("\nfuse_fgetattr(path=\"%s\", statbuf=0x%08x, fi=0x%08x)\n",
	    path, statbuf, fi);
    log_fi(fi);
    fd = cache_get_fd((unsigned long)fi->fh);
	if (fd >=0) {
    	retstat = fstat(fd, statbuf);
    	if (retstat < 0) {
			retstat = err_msg("fuse_fgetattr fstat");
			ioctl_msg(NULL, "error in fuse_fgetattr\n");
		}
	}
	else 
		err_msg("unable to convert bd to fs %xl",(unsigned long)fi->fh);
    
    log_stat(statbuf);
    
    return retstat;
}
/** Read the target of a symbolic link
 *
 * The buffer should be filled with a null terminated string.  The
 * buffer size argument includes the space for the terminating
 * null character.  If the linkname is too long to fit in the
 * buffer, it should be truncated.  The return value should be 0
 * for success.
 */
int bb_readlink(const char *path, char *link, size_t size)
{
    int retstat = 0;
    char fpath[PATH_MAX];
    
    log_msg("bb_readlink(path=\"%s\", link=\"%s\", size=%d)\n",
	  path, link, size);
    fuse_fullpath(fpath, path);
    
    retstat = readlink(fpath, link, size - 1);
    if (retstat < 0)
		retstat = bb_error((char*)"bb_readlink readlink");
    else  {
		link[retstat] = '\0';
		retstat = 0;
    }
    
    return retstat;
}
/** Create a file node
 *
 * There is no create() operation, mknod() will be called for
 * creation of all non-directory, non-symlink nodes.
 */
int bb_mknod(const char *path, mode_t mode, dev_t dev)
{
    int retstat = 0;
    char fpath[PATH_MAX];
    
    log_msg("\nbb_mknod(path=\"%s\", mode=0%3o, dev=%lld)\n",
	  path, mode, dev);
    fuse_fullpath(fpath, path);
    
    // On Linux this could just be 'mknod(path, mode, rdev)' but this
    //  is more portable
    if (S_ISREG(mode)) {
        retstat = open(fpath, O_CREAT | O_EXCL | O_WRONLY, mode);
	if (retstat < 0)
	    retstat = bb_error((char*)"bb_mknod open");
        else {
            retstat = close(retstat);
	    if (retstat < 0)
		retstat = bb_error((char*)"bb_mknod close");
	}
    } else
	if (S_ISFIFO(mode)) {
	    retstat = mkfifo(fpath, mode);
	    if (retstat < 0)
		retstat = bb_error((char*)"bb_mknod mkfifo");
	} else {
	    retstat = mknod(fpath, mode, dev);
	    if (retstat < 0)
		retstat = bb_error((char*)"bb_mknod mknod");
	}
    
    return retstat;
}
/** Create a directory */
int bb_mkdir(const char *path, mode_t mode)
{
    int retstat = 0;
    char fpath[PATH_MAX];
    //printf ("@@@@@@@@@@@@@@@@@@ mkdir @@@@@@@@@@@@@");
    log_msg("\nbb_mkdir(path=\"%s\", mode=0%3o)\n",
	    path, mode);
    fuse_fullpath(fpath, path);
    
    retstat = mkdir(fpath, mode);
    if (retstat < 0)
	retstat = bb_error((char*)"bb_mkdir mkdir");
    
    return retstat;
}
/** Remove a file */
int bb_unlink(const char *path)
{
    int retstat = 0;
    char fpath[PATH_MAX];
    
    log_msg("bb_unlink(path=\"%s\")\n",
	    path);
    fuse_fullpath(fpath, path);
    
    retstat = unlink(fpath);
    if (retstat < 0)
	retstat = bb_error((char*)"bb_unlink unlink");
    
    return retstat;
}
/** Remove a directory */
int bb_rmdir(const char *path)
{
    int retstat = 0;
    char fpath[PATH_MAX];
    
    log_msg("bb_rmdir(path=\"%s\")\n",
	    path);
    fuse_fullpath(fpath, path);
    
    retstat = rmdir(fpath);
    if (retstat < 0)
	retstat = bb_error((char*)"bb_rmdir rmdir");
    
    return retstat;
}
/** Create a symbolic link */
int bb_symlink(const char *path, const char *link)
{
    int retstat = 0;
    char flink[PATH_MAX];
    
    log_msg("\nbb_symlink(path=\"%s\", link=\"%s\")\n",
	    path, link);
    fuse_fullpath(flink, link);
    
    retstat = symlink(path, flink);
    if (retstat < 0)
	retstat = bb_error((char*)"bb_symlink symlink");
    
    return retstat;
}
/** Create a hard link to a file */
int bb_link(const char *path, const char *newpath)
{
    int retstat = 0;
    char fpath[PATH_MAX], fnewpath[PATH_MAX];
    
    log_msg("\nbb_link(path=\"%s\", newpath=\"%s\")\n",
	    path, newpath);
    fuse_fullpath(fpath, path);
    fuse_fullpath(fnewpath, newpath);
    
    retstat = link(fpath, fnewpath);
    if (retstat < 0)
	retstat = bb_error((char*)"bb_link link");
    
    return retstat;
}
/** Change the permission bits of a file */
int bb_chmod(const char *path, mode_t mode)
{
    int retstat = 0;
    char fpath[PATH_MAX];
    
    log_msg("\nbb_chmod(fpath=\"%s\", mode=0%03o)\n",
	    path, mode);
    fuse_fullpath(fpath, path);
    
    retstat = chmod(fpath, mode);
    if (retstat < 0)
	retstat = bb_error((char*)"bb_chmod chmod");
    
    return retstat;
}

/** Change the owner and group of a file */
int bb_chown(const char *path, uid_t uid, gid_t gid)
  
{
    int retstat = 0;
    char fpath[PATH_MAX];
    
    log_msg("\nbb_chown(path=\"%s\", uid=%d, gid=%d)\n",
	    path, uid, gid);
    fuse_fullpath(fpath, path);
    
    retstat = chown(fpath, uid, gid);
    if (retstat < 0)
	retstat = bb_error((char*)"bb_chown chown");
    
    return retstat;
}
/** Change the size of a file */
int bb_truncate(const char *path, off_t newsize)
{
    int retstat = 0;
    char fpath[PATH_MAX];
    
    log_msg("\nbb_truncate(path=\"%s\", newsize=%lld)\n",
	    path, newsize);
    fuse_fullpath(fpath, path);
    
    retstat = truncate(fpath, newsize);
    if (retstat < 0)
	bb_error((char*)"bb_truncate truncate");
    
    return retstat;
}

/** Write data to an open file
 *
 * Write should return exactly the number of bytes requested
 * except on error.  An exception to this is when the 'direct_io'
 * mount option is specified (see read operation).
 *
 * Changed in version 2.2
 */
int bb_write(const char *path, const char *buf, size_t size, off_t offset,
	     struct fuse_file_info *fi)
{
    int retstat = 0;
    
    log_msg("\nbb_write(path=\"%s\", buf=0x%08x, size=%d, offset=%lld, fi=0x%08x)\n",
	    path, buf, size, offset, fi
	    );
    // no need to get fpath on this one, since I work from fi->fh not the path
    log_fi(fi);
	
    retstat = pwrite(fi->fh, buf, size, offset);
    if (retstat < 0)
	retstat = bb_error((char*)"bb_write pwrite");
    
    return retstat;
}
/** Get file system statistics
 *
 * The 'f_frsize', 'f_favail', 'f_fsid' and 'f_flag' fields are ignored
 *
 * Replaced 'struct statfs' parameter with 'struct statvfs' in
 * version 2.5
 */
int bb_statfs(const char *path, struct statvfs *statv)
{
    int retstat = 0;
    char fpath[PATH_MAX];
    
    log_msg("\nbb_statfs(path=\"%s\", statv=0x%08x)\n",
	    path, statv);
    fuse_fullpath(fpath, path);
    
    // get stats for underlying filesystem
    retstat = statvfs(fpath, statv);
    if (retstat < 0)
	retstat = bb_error((char*)"bb_statfs statvfs");
    
    log_statvfs(statv);
    
    return retstat;
}
/** Synchronize file contents
 *
 * If the datasync parameter is non-zero, then only the user data
 * should be flushed, not the meta data.
 *
 * Changed in version 2.2
 */
int bb_fsync(const char *path, int datasync, struct fuse_file_info *fi)
{
    int retstat = 0;
    
    log_msg("\nbb_fsync(path=\"%s\", datasync=%d, fi=0x%08x)\n",
	    path, datasync, fi);
    log_fi(fi);
    
    if (datasync)
	retstat = fdatasync(fi->fh);
    else
	retstat = fsync(fi->fh);
    
    if (retstat < 0)
	bb_error((char*)"bb_fsync fsync");
    
    return retstat;
}
/** Set extended attributes */
int bb_setxattr(const char *path, const char *name, const char *value, size_t size, int flags)
{
    int retstat = 0;
    char fpath[PATH_MAX];
    
    log_msg("\nbb_setxattr(path=\"%s\", name=\"%s\", value=\"%s\", size=%d, flags=0x%08x)\n",
	    path, name, value, size, flags);
    fuse_fullpath(fpath, path);
    
    retstat = lsetxattr(fpath, name, value, size, flags);
    if (retstat < 0)
	retstat = bb_error((char*)"bb_setxattr lsetxattr");
    
    return retstat;
}
/** Get extended attributes */
int bb_getxattr(const char *path, const char *name, char *value, size_t size)
{
    int retstat = 0;
    char fpath[PATH_MAX];
    
    log_msg("\nbb_getxattr(path = \"%s\", name = \"%s\", value = 0x%08x, size = %d)\n",
	    path, name, value, size);
    fuse_fullpath(fpath, path);
    
    retstat = lgetxattr(fpath, name, value, size);
    if (retstat < 0)
	retstat = bb_error((char*)"bb_getxattr lgetxattr");
    else
	log_msg("    value = \"%s\"\n", value);
    
    return retstat;
}
/** List extended attributes */
int bb_listxattr(const char *path, char *list, size_t size)
{
    int retstat = 0;
    char fpath[PATH_MAX];
    char *ptr;
    
    log_msg("bb_listxattr(path=\"%s\", list=0x%08x, size=%d)\n",
	    path, list, size
	    );
    fuse_fullpath(fpath, path);
    
    retstat = llistxattr(fpath, list, size);
    if (retstat < 0)
	retstat = bb_error((char*)"bb_listxattr llistxattr");
    
    log_msg("    returned attributes (length %d):\n", retstat);
    for (ptr = list; ptr < list + retstat; ptr += strlen(ptr)+1)
	log_msg("    \"%s\"\n", ptr);
    
    return retstat;
}
/** Remove extended attributes */
int bb_removexattr(const char *path, const char *name)
{
    int retstat = 0;
    char fpath[PATH_MAX];
    
    log_msg("\nbb_removexattr(path=\"%s\", name=\"%s\")\n",
	    path, name);
    fuse_fullpath(fpath, path);
    
    retstat = lremovexattr(fpath, name);
    if (retstat < 0)
	retstat = bb_error((char*)"bb_removexattr lrmovexattr");
    
    return retstat;
}
int bb_fsyncdir(const char *path, int datasync, struct fuse_file_info *fi)
{
    int retstat = 0;
    
    log_msg("\nbb_fsyncdir(path=\"%s\", datasync=%d, fi=0x%08x)\n",
	    path, datasync, fi);
    log_fi(fi);
    
    return retstat;
}
int bb_create(const char *path, mode_t mode, struct fuse_file_info *fi)
{
    int retstat = 0;
    char fpath[PATH_MAX];
    int fd;
    
    log_msg("\nbb_create(path=\"%s\", mode=0%03o, fi=0x%08x)\n",
	    path, mode, fi);
    fuse_fullpath(fpath, path);
    
    fd = creat(fpath, mode);
    if (fd < 0)
	retstat = bb_error((char*)"bb_create creat");
    
    fi->fh = fd;
    
    log_fi(fi);
    
    return retstat;
}
int bb_ftruncate(const char *path, off_t offset, struct fuse_file_info *fi)
{
    int retstat = 0;
    
    log_msg("\nbb_ftruncate(path=\"%s\", offset=%lld, fi=0x%08x)\n",
	    path, offset, fi);
    log_fi(fi);
    
    retstat = ftruncate(fi->fh, offset);
    if (retstat < 0)
	retstat = bb_error((char*)"bb_ftruncate ftruncate");
    
    return retstat;
}

void fuse_set_operations()
{
	// Initialise the operations
	caching_oper.getattr = fuse_getattr;//no changes needed
	caching_oper.access = fuse_access;//no changes needed
	caching_oper.open = fuse_open;//implemented by my cache
	caching_oper.read = fuse_read;//implemented by my cache
	caching_oper.flush = fuse_flush;//no changes needed
	caching_oper.release = fuse_release;//implemented by my cache
	caching_oper.opendir = fuse_opendir;//no changes needed
	caching_oper.readdir = fuse_readdir;//no changes needed
	caching_oper.releasedir = fuse_releasedir;//no changes needed
	caching_oper.rename = fuse_rename; //raise error msg, if the file it open TODO: need to update the cache
	caching_oper.init = cache_init;//implemented by my cache
	caching_oper.destroy = fuse_destroy; //implemented by my cache
	caching_oper.ioctl = fuse_ioctl;//implemented by my cache
	caching_oper.fgetattr = fuse_fgetattr;//implemented by my cache retrive fd from block_cache

	caching_oper.readlink = bb_readlink;
	caching_oper.getdir = NULL;
	caching_oper.mknod = bb_mknod;
	caching_oper.mkdir = bb_mkdir;
	caching_oper.unlink = bb_unlink;
	caching_oper.rmdir = bb_rmdir;
	caching_oper.symlink = bb_symlink;
	caching_oper.link = bb_link;
	caching_oper.chmod = bb_chmod;
	caching_oper.chown = bb_chown;
	caching_oper.truncate = bb_truncate;
	caching_oper.utime = fuse_utime;
	caching_oper.write = bb_write;
	caching_oper.statfs = bb_statfs;
	caching_oper.fsync = bb_fsync;
	caching_oper.setxattr = bb_setxattr;
	caching_oper.getxattr = bb_getxattr;
	caching_oper.listxattr = bb_listxattr;
	caching_oper.removexattr = bb_removexattr;
	caching_oper.fsyncdir = bb_fsyncdir;
	caching_oper.create = bb_create;
	caching_oper.ftruncate = bb_ftruncate;
};

void fuse_usage()
{
    fprintf(stdout, "usage:  myCachFileSystem rootDir mountPoint numOfBlocks blockSize\n");
    exit(1);
}

int main(int argc, char *argv[])
{
    int fuse_stat;
    struct myCache_state *fuse_data;
	char *mountdir=NULL;

    if ((getuid() == 0) || (geteuid() == 0)) {
	//fprintf(stderr, "Running caching as root opens unnacceptable security holes\n");
	return 1;
    }

    if ((argc < 5) || (argv[argc-2][0] == '-') || (argv[argc-1][0] == '-'))
	fuse_usage();

    fuse_data = (myCache_state *)malloc(sizeof(struct myCache_state));
    if (fuse_data == NULL) {
	//perror("main calloc");
	abort();
    }
	#ifdef MY_DEBUG
    	fuse_data->rootdir = realpath(argv[1], NULL);
		mountdir = argv[2];
	#else
		fuse_data->rootdir = (char*)malloc(PATH_MAX);
		strcpy(fuse_data->rootdir, "/tmp/");
		strcat(fuse_data->rootdir, argv[1]);
		mountdir = (char*)malloc(PATH_MAX);
		strcpy(mountdir, "/tmp/");
		strcat(mountdir, argv[2]);
	#endif
	fuse_data->numBlocks= atoi(argv[3]);
	fuse_data->blockSize = atoi(argv[4]);
	argv[1] = mountdir;
	argv[2] = NULL;
	argv[3] = NULL;
	argv[4] = NULL;
    argc=2;
    
    fuse_data->logfile = log_open();
    
	fuse_set_operations();

    // turn over control to fuse
    //fprintf(stderr, "about to call fuse_main rootdir=%s numBlocks=%u blockSize=%u\n", 
		//			fuse_data->rootdir, fuse_data->numBlocks, fuse_data->blockSize);
	fuse_stat = fuse_main(argc, argv, &caching_oper, fuse_data);
	
	//fprintf(stderr, "fuse_main returned %d\n", fuse_stat);
   	if (fuse_data->rootdir != NULL)
		free(fuse_data->rootdir);
	#ifndef  MY_DEBUG
		if (mountdir != NULL)
			free(mountdir);
	#endif
    return fuse_stat;
}
